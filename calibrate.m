function [ calibrated ] = calibrate( data,time,ls_range,order )
%CALIBRATE range is a 1x2 vector [start end]
%   outputs full range calibrated data that was calibrated over given range
%   default is second order
%   EXAMPLE: Accel.X = calibrate(Accel.X,Accel.Time,[1,20000],2);
       
if exist('order','var') ~= 1
    order = 2;
end

% ls_data = data(ls_range(1):ls_range(2));
% ls_time = time(ls_range(1):ls_range(2));
ls_data = data(ls_range);
ls_time = time(ls_range);
coeff_a = LeastSq(ls_data,ls_time,order);

calibrated = data - (coeff_a(1) + coeff_a(2).*time);
end

