    dt = Gyro.Time(2)-Gyro.Time(1);
    roll = 0;
%      Gyro.X = Gyro.X - mean(Gyro.X (50:200));
     Gyro.X = calibrate(Gyro.X,Gyro.Time,stillRange);

    for c=1:length(Gyro.X)-1
        if Gyro.Time(c) < 2
            Gyro.X(c) = 0;
        end
        roll(c) = roll(c)*ismoving(c);
        roll(c+1) =  roll(c)+Gyro.X(c)*dt;
    end