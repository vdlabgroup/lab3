%This is a script to save all the data files into uniform and easily usable
% structures to make working with them easier

clc, clear all
toClean = 'Leo1';
load (['MAT files/',toClean,'.mat'])
clear ('time')
%First im going to clean CANBUS data
CANBUS.Time            = gG35_2_can.time;
CANBUS.engineTorque    = gG35_2_can.Estimated_Engine_Torque;
CANBUS.OmegaFR         = gG35_2_can.Wheel_SpeedFR;
CANBUS.OmegaFL         = gG35_2_can.Wheel_SpeedFL;
CANBUS.OmegaRR         = gG35_2_can.Wheel_SpeedRR;
CANBUS.OmegaRL         = gG35_2_can.Wheel_SpeedRL;
CANBUS.SteerAngle      = gG35_2_can.SteerAngle;
CANBUS.SteerAngleVel   = gG35_2_can.Steering_Angle_Velocity;
CANBUS.LongAccel       = gG35_2_can.zLongitudinalAcceleration;
CANBUS.TransAccel      = gG35_2_can.zTransversalAccleration;
CANBUS.YawRate         = gG35_2_can.zYawRate_CAN;
CANBUS.RPM             = gG35_2_can.EngineRPM;

clear('gG35_2_can')

%Next im going to clean the accelerometer data
Accelerometer.Time          = gXbow400.time;
Accelerometer.X             = gXbow400.zAccelX;
Accelerometer.Y             = gXbow400.zAccelY;
Accelerometer.Z             = gXbow400.zAccelZ;

Gyrometer.time           = gXbow400.time;
Gyrometer.X              = gXbow400.zGyroX;
Gyrometer.Y              = gXbow400.zGyroY;
Gyrometer.Z              = gXbow400.zGyroZ;

clear('gXbow400')

%Finally, im going to clean the GPS data
GPS.Time            = gSeptentrio.time;
GPS.Number          = gSeptentrio.zNumSats_PVT;
GPS.Yaw             = gSeptentrio.zYaw;
GPS.YawRate         = gSeptentrio.zYaw_Var;
GPS.Pitch           = gSeptentrio.zPitch;
GPS.PitchRate       = gSeptentrio.zPitch_Var;
GPS.Roll            = gSeptentrio.zRoll;
GPS.RollRate        = gSeptentrio.zRoll_Var;
GPS.X               = gSeptentrio.zECEF_X;
GPS.Y               = gSeptentrio.zECEF_Y;
GPS.Z               = gSeptentrio.zECEF_Z;
GPS.VX              = gSeptentrio.zECEF_VX;
GPS.VY              = gSeptentrio.zECEF_VY;
GPS.VZ              = gSeptentrio.zECEF_VZ;

clear ('gSeptentrio')

%The averate time step for CAN is somewhere around .001 seconds, so we need
%to make everything, including CAN stuff on that time step
close all
dt = .001;

%% CAN Cleaning
c = 1;
a.Torque        = 1;
a.OmegaFR       = 3;
a.OmegaFL       = 3;
a.OmegaRR       = 3;
a.OmegaRL       = 3;
a.SteerAngle    = 1;
a.SteerAngleVel = 1;
a.LongAccel     = 1;
a.TransAccel    = 1;
a.YawRate       = 1;
a.RPM           = 1;

CANBUS.Time = CANBUS.Time - CANBUS.Time(1);
Can.Time = [0:dt:CANBUS.Time(length(CANBUS.RPM))];
% Getting rid of all the NaN values
while c < length(CANBUS.LongAccel)
    if isfinite(CANBUS.engineTorque(c))
        CanBus.EngineTorque(1,a.Torque) = CANBUS.engineTorque(c);
        CanBus.EngineTorque(2,a.Torque) = CANBUS.Time(c);
        a.Torque = a.Torque+1;
    end
    CanBus.OmegaFR(1,1) = 0;
    CanBus.OmegaFR(2,1) = 0;
    CanBus.OmegaFL(2,1) = 0;
    CanBus.OmegaFL(1,1) = 0;
    CanBus.OmegaRR(2,1) = 0;
    CanBus.OmegaRR(1,1) = 0;
    CanBus.OmegaRL(2,1) = 0;
    CanBus.OmegaRL(1,1) = 0;
    
    
    if isfinite(CANBUS.OmegaFR(c)) && CANBUS.OmegaFR(c) ~= 0
        CanBus.OmegaFR(1,a.OmegaFR) = CANBUS.OmegaFR(c);
        CanBus.OmegaFR(2,a.OmegaFR) = CANBUS.Time(c);
        if a.OmegaFR ==3
        CanBus.OmegaFR(1,a.OmegaFR-1) = 0;
        CanBus.OmegaFR(2,a.OmegaFR-1) = CANBUS.Time(c-1);  
        end
        a.OmegaFR = a.OmegaFR+1;
    end
    if isfinite(CANBUS.OmegaFL(c)) && CANBUS.OmegaFL(c) ~= 0
        CanBus.OmegaFL(1,a.OmegaFL) = CANBUS.OmegaFL(c);
        CanBus.OmegaFL(2,a.OmegaFL) = CANBUS.Time(c);
        if a.OmegaFL ==3
        CanBus.OmegaFL(1,a.OmegaFL-1) = 0;
        CanBus.OmegaFL(2,a.OmegaFL-1) = CANBUS.Time(c-1);  
        end
        a.OmegaFL = a.OmegaFL+1;
    end
    if isfinite(CANBUS.OmegaRR(c)) && CANBUS.OmegaRR(c) ~= 0
        CanBus.OmegaRR(1,a.OmegaRR) = CANBUS.OmegaRR(c);
        CanBus.OmegaRR(2,a.OmegaRR) = CANBUS.Time(c);
        if a.OmegaRR ==3
        CanBus.OmegaRR(1,a.OmegaRR-1) = 0;
        CanBus.OmegaRR(2,a.OmegaRR-1) = CANBUS.Time(c-1);  
        end
        a.OmegaRR = a.OmegaRR+1;
    end
    if isfinite(CANBUS.OmegaRL(c)) && CANBUS.OmegaRL(c) ~= 0
        CanBus.OmegaRL(1,a.OmegaRL) = CANBUS.OmegaRL(c);
        CanBus.OmegaRL(2,a.OmegaRL) = CANBUS.Time(c);
        if a.OmegaRL ==3
        CanBus.OmegaRL(1,a.OmegaRL-1) = 0;
        CanBus.OmegaRL(2,a.OmegaRL-1) = CANBUS.Time(c-1);  
        end
        a.OmegaRL = a.OmegaRL+1;
    end
    if isfinite(CANBUS.SteerAngle(c)) && CANBUS.SteerAngle(c) ~= 0
        CanBus.SteerAngle(1,a.SteerAngle) = CANBUS.SteerAngle(c);
        CanBus.SteerAngle(2,a.SteerAngle) = CANBUS.Time(c);
        a.SteerAngle = a.SteerAngle+1;
    end
    if isfinite(CANBUS.SteerAngleVel(c))
        CanBus.SteerAngleVel(1,a.SteerAngleVel) = CANBUS.SteerAngleVel(c);
        CanBus.SteerAngleVel(2,a.SteerAngleVel) = CANBUS.Time(c);
        a.SteerAngleVel = a.SteerAngleVel+1;
    end
    if isfinite(CANBUS.LongAccel(c))
        CanBus.LongAccel(1,a.LongAccel) = CANBUS.LongAccel(c);
        CanBus.LongAccel(2,a.LongAccel) = CANBUS.Time(c);
        a.LongAccel = a.LongAccel+1;
    end
    if isfinite(CANBUS.TransAccel(c))
        CanBus.TransAccel(1,a.TransAccel) = CANBUS.TransAccel(c);
        CanBus.TransAccel(2,a.TransAccel) = CANBUS.Time(c);
        a.TransAccel = a.TransAccel+1;
    end
    if isfinite(CANBUS.YawRate(c))
        CanBus.YawRate(1,a.YawRate) = CANBUS.YawRate(c);
        CanBus.YawRate(2,a.YawRate) = CANBUS.Time(c);
        a.YawRate = a.YawRate+1;
    end
    if isfinite(CANBUS.RPM(c))
        CanBus.RPM(1,a.RPM) = CANBUS.RPM(c);
        CanBus.RPM(2,a.RPM) = CANBUS.Time(c);
        a.RPM = a.RPM+1;
    end
    c=c+1;
end

% Filling in the gaps with interpolation
Can.EngineTorque = interp1(CanBus.EngineTorque(2,:),CanBus.EngineTorque(1,:),Can.Time,'linear');
Can.OmegaFR      = interp1(CanBus.OmegaFR(2,:),CanBus.OmegaFR(1,:),Can.Time,'linear');
Can.OmegaFL      = interp1(CanBus.OmegaFL(2,:),CanBus.OmegaFL(1,:),Can.Time,'linear');
Can.OmegaRR      = interp1(CanBus.OmegaRR(2,:),CanBus.OmegaRR(1,:),Can.Time,'linear');
Can.OmegaRL      = interp1(CanBus.OmegaRL(2,:),CanBus.OmegaRL(1,:),Can.Time,'linear');
Can.SteerAngle   = interp1(CanBus.SteerAngle(2,:),CanBus.SteerAngle(1,:),Can.Time,'linear');
Can.SteerAngleVel= interp1(CanBus.SteerAngleVel(2,:),CanBus.SteerAngleVel(1,:),Can.Time,'linear');
Can.LongAccel    = interp1(CanBus.LongAccel(2,:),CanBus.LongAccel(1,:),Can.Time,'linear');
Can.TransAccel   = interp1(CanBus.TransAccel(2,:),CanBus.TransAccel(1,:),Can.Time,'linear');
Can.YawRate      = interp1(CanBus.YawRate(2,:),CanBus.YawRate(1,:),Can.Time,'linear');
Can.RPM          = interp1(CanBus.RPM(2,:),CanBus.RPM(1,:),Can.Time,'linear');

clear('a')
%% Accelerometer Cleaning
c = 1;
a.Ax       = 1;
a.Ay       = 1;
a.Az       = 1;
a.Gx       = 1;
a.Gy       = 1;
a.Gz       = 1;

Accelerometer.Time = Accelerometer.Time - Accelerometer.Time(1);
Gyrometer.time = Gyrometer.time - Gyrometer.time(1);
Accel.Time = [0:dt:CANBUS.Time(length(CANBUS.RPM))];
Gyro.Time  = [0:dt:CANBUS.Time(length(CANBUS.RPM))];

% Getting rid of all the NaN values
while c < length(Accelerometer.X)
    if isfinite(Accelerometer.X(c))
        accelerometer.X(1,a.Ax) = Accelerometer.X(c);
        accelerometer.X(2,a.Ax) = Accelerometer.Time(c);
        a.Ax = a.Ax+1;
    end
    if isfinite(Accelerometer.Y(c))
        accelerometer.Y(1,a.Ay) = Accelerometer.Y(c);
        accelerometer.Y(2,a.Ay) = Accelerometer.Time(c);
        a.Ay = a.Ay+1;
    end
    if isfinite(Accelerometer.Z(c))
        accelerometer.Z(1,a.Az) = Accelerometer.Z(c);
        accelerometer.Z(2,a.Az) = Accelerometer.Time(c);
        a.Az = a.Az+1;
    end
    c = c+1;
end
c = 1;
while c < length(Gyrometer.X)
    if isfinite(Gyrometer.X(c))
        gyrometer.X(1,a.Gx) = Gyrometer.X(c,1);
        gyrometer.X(2,a.Gx) = Gyrometer.time(c,1);
        a.Gx = a.Gx+1;
    end
    if isfinite(Gyrometer.Y(c))
        gyrometer.Y(1,a.Gy) = Gyrometer.Y(c,1);
        gyrometer.Y(2,a.Gy) = Gyrometer.time(c,1);
        a.Gy = a.Gy+1;
    end
    if isfinite(Gyrometer.Z(c))
        gyrometer.Z(1,a.Gz) = Gyrometer.Z(c,1);
        gyrometer.Z(2,a.Gz) = Gyrometer.time(c,1);
        a.Gz = a.Gz+1;
    end
    c = c+1;
end
c = 1;

Accel.X      = interp1(accelerometer.X(2,:),accelerometer.X(1,:),Accel.Time,'linear');
Accel.Y      = interp1(accelerometer.Y(2,:),accelerometer.Y(1,:),Accel.Time,'linear');
Accel.Z      = interp1(accelerometer.Z(2,:),accelerometer.Z(1,:),Accel.Time,'linear');

Gyro.X       = interp1(gyrometer.X(2,:),gyrometer.X(1,:),Gyro.Time,'linear');
Gyro.Y       = interp1(gyrometer.Y(2,:),gyrometer.Y(1,:),Gyro.Time,'linear');
Gyro.Z       = interp1(gyrometer.Z(2,:),gyrometer.Z(1,:),Gyro.Time,'linear');
clear('a','Accelerometer','accelerometer','Gyrometer','gyrometer','CanBus','CANBUS');
clear('c','dt')

save(['Clean/',toClean,'.mat'],'Accel','Can','GPS','Gyro');


