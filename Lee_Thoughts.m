%Lee stream of consciousness
clc, close all, clear all
range = (210000: 250000);
stillRange = 80:40000;
showPlots =1;
load('Clean/Leo2')
load('Raw_Data_And_Parsing/Clean/Leo2')
dt = Accel.Time(2)-Accel.Time(1);


figure
hold on;
plot(Accel.Time(range),Accel.X(range))

% calibrate
Accel.X(1:80) = mean(Accel.X(stillRange));
Accel.X = calibrate(Accel.X,Accel.Time,stillRange);
plot(Accel.Time(range),Accel.X(range),'-.')


time = Accel.Time;
tic
%We have semi decent yaw data:

yawRate = zeros(size(time));
run('lee_thoughts/getYawRate')
if showPlots
    plot1 = figure;
    plot(time(range),yawRate(range))
    title('Yaw Rate as a function of Time')
end

AvgOmega = zeros(size(time));
ismoving = zeros(size(time));
%We also have usable Velocity data:
run('lee_thoughts/getVelOmega')
run('lee_thoughts/getVelocity')
if showPlots
    plot2 = figure;
    plot(time(range),VelocityOmega(range));%,RPM2RadPerSec*radius*Can.OmegaFL(range),':')
    hold on
    plot (time(range),Velocity.X(range))
    title('Velocity X as a function of Time')
    xlabel('Time(s)');ylabel('Velocity X (m/s)');
    legend('Via WheelSpeed','Via integration')
  
end

CentA = Velocity.X.*yawRate/9.81; % cent a in G's

if showPlots
    plot3 = figure;
    plot(time(range),CentA(range))
    hold on
    Accel.Y = calibrate(Accel.Y,Accel.Time,stillRange);
    plot (time(range), Accel.Y(range)/9.81,':')
    title ('Centripital Acceleration as a function of time')
    xlabel('Time(s)');ylabel('Centripital Acceleration(gs)');
end

roll = zeros(size(time));

run('lee_thoughts/findRoll')
roll = roll*180/pi;
if showPlots
    plot4 = figure;
    plot(time(range),roll(range))
    title ('Roll as a function of time')
    xlabel('Time(s)');ylabel('Roll angle (deg)');
end

rollRate = zeros(size(time));
a = 1;
for c = 1:length(roll)
    if abs(roll(c))> 0.01 && abs(CentA(c))>.025
        plotr.roll(a) = roll(c);
        plotr.Accel(a) = CentA(c);
        a = a+1;
    end
end
for c = 1:length(roll)
    if abs(CentA(c)) < 0.1
        rollRate(c) = 0;
    else
        rollRate(c) = abs(roll(c)/CentA(c));
    end
end
b = 1;
for c = range
    if isfinite(rollRate(c)) && rollRate(c) >0.1
        
        rollStiffness(b) = rollRate(c);
        b = b+1;
    end
end
mean(rollStiffness)
plot5 = figure;
plot (time(range),rollRate(range))
title ('Roll Rate over time')
xlabel('Time(s)');ylabel('Roll Rate (deg/G)');
% [A,B] = min(plotr.roll);
% A/plotr.Accel(B)
% [C,D] = max(plotr.roll);
% C/plotr.Accel(D)
toc