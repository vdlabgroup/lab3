function [coeffs, line] = LeastSq (data, time, order)
%% [ coeffs, line ] = LeastSq (time, data, order)
% be sure data is truncated to still portion before entering
% outputs coefficients of 1,t,t^2,..,t^n
% also can output data needed to plot the 'best fit' line using the 
% least squared coefficients

%% Setup data
[rowd cold] = size(data);

if rowd < cold
    data = data';
end

%% Compute Least Squares
Ones =  ones(length(time),1);
H(:,1) = Ones;

for n = 2:order
H(:,n) = time.^(n-1);
end

xHat = pinv(H)*data;
% xHat = (transpose(H)*H)\transpose(H)*data;

for n = 1:order
coeffs(n) = xHat(n); 
end

%% Solve for line data
line = ones(length(time),order);
line(:,1) = 1;
for n = 2:order
    line(:,n) = (time.^(n-1));
end
line = line*coeffs';