%LateralAcceleration
clc, clear all,close all
load('Raw_Data_And_Parsing/Clean/Leo1')

% Calibrate data
Accel.X = calibrate(Accel.X,Accel.Time,35000:60000);
Accel.Y = calibrate(Accel.Y,Accel.Time,35000:60000);

% Select data from skidpad test
range = 89220:105000;

Accel.Time = Accel.Time(range);
Accel.X = Accel.X(range);
Accel.Y = Accel.Y(range);
Can.SteerAngle = Can.SteerAngle(range)-90;
Can.Time = Can.Time(range);

% Filter aY
Accel.Y = smooth(Accel.Y,180); %change to butter

% Loop for getting velocity
dt = Accel.Time(2)-Accel.Time(1);
g = 9.81;
R = 16.70;
Velocity.X = 0;
for c = 1:length(Accel.Time)
   Velocity.X(c+1)  = Velocity.X(c) + Accel.X(c) * dt;
end
Velocity.X(c+1) = [];
AySS = (Velocity.X.^2)./R;

% get kus
linearRange = [1:13500];
[coeffs line] = LeastSq(Can.SteerAngle(linearRange)/15.9,AySS(linearRange)/g,2);
kus = coeffs(2);
str = sprintf('kus = %2.2f',kus);

%% Plot results
figure('Name','LateralAccel','Position',[1.8000   41.8000  766.4000  740.8000]);
ha(1) = subplot(3,1,1);
plot(Accel.Time,Accel.Y/g); hold on;
plot(Accel.Time,AySS/g);
grid on; xlabel('time (s)'); ylabel('lateral accel (g''s)');
sensTxt = sprintf('Sensor: max = %2.2f (g''s)',max(abs(Accel.Y))/g);
ssTxt = sprintf('V^2/R: max = %2.2f (g''s)',max(abs(AySS))/g);
legend(sensTxt,ssTxt,'Location','NorthWest');

ha(2) = subplot(3,1,2); 
plot(Can.Time,Can.SteerAngle/15.9);
grid on; xlabel('time (s)'); ylabel('steer ang (units)');
titletxt = sprintf('max steer angle = %2.2f',max(abs(Can.SteerAngle))/15.9);
title(titletxt);

ha(3) = subplot(3,1,3); 
plot(Accel.Time,Velocity.X);
grid on; xlabel('time (s)'); ylabel('velocity (m/s)');
linkaxes(ha, 'x');

figure('Name','steer','Position',[769.80,41.800,766.40,740.80]);
hold on; grid on;
plot(Accel.Y/g,Can.SteerAngle/15.9,'*');
plot(AySS/g,Can.SteerAngle/15.9,'*');

plot(AySS(linearRange)/g,line,'k');
legend('accelerometer','v^2/R',str);
xlabel('lateral accel (g''s)'); ylabel('steer angle (deg)');
