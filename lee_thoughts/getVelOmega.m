%Get Velocity Via omega
for c = 1:length(Can.OmegaFR)
AvgOmega(c) = mean([Can.OmegaFL(c),Can.OmegaRR(c),Can.OmegaRL(c)]);
if AvgOmega(c) <.2
    ismoving(c) = 0;
else
    ismoving(c) =1;
end
end

radius = .35;
RPM2RadPerSec = 2*pi/60;
VelocityOmega = AvgOmega*radius*RPM2RadPerSec;