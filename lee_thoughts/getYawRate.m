%Get Yaw Rate
d1 = designfilt('lowpassiir','FilterOrder',12, ...
    'HalfPowerFrequency',0.15,'DesignMethod','butter');
Gyro.Z = filtfilt(d1,Gyro.Z);
dt = Gyro.Time(2)-Gyro.Time(1);
heading = 0;
for c=1:length(Gyro.Z)
    if Gyro.Time(c) < 2
        Gyro.Z(c) = 0;
    end
    if c >6
        if abs(Gyro.Z(c))<.025 && abs(Gyro.Z(c-5))<.05
            Gyro.Z(c) = 0;
        end
    end
    heading(c+1) =  heading(c)+Gyro.Z(c)*dt;
end
yawRate = Gyro.Z;
