%DeadReckoning
clc, clear all,close all
tic
load('Clean/David1')

%% Looping
dt = Accel.Time(2)-Accel.Time(1);
Velocity.X = 0;
Velocity.Y = 0;
Heading = 0;
Position.North = 0;
Position.East  = 0;

% DataForLeastSqr.A = Accel.X(5:2000);
% time_ = Accel.Time(5:2000);
% coeff_a = LeastSq(DataForLeastSqr.A,time_,2);
% Accel.X = Accel.X-(coeff_a(1) + coeff_a(2).*Accel.X);

Accel.X(1:50) = 0;
offset.X = mean(Accel.X(50:500));
Accel.X(50:end) = Accel.X(50:end)-offset.X;

for c = 1:length(Accel.Time)
    if abs(Accel.X(c)) < .25
        Accel.X(c) = 0;
    end
    Velocity.X(c+1)  = Velocity.X(c) + Accel.X(c) * dt;
    Heading(c+1)     = Heading(c) + Gyro.Z (c)*dt;
end

Accel.YminusCent = Accel.Y - Velocity.X(1:end-1) .* Gyro.Z;

for c = 1:length(Accel.Time)
    Velocity.Y(c+1)  = Velocity.Y(c) +Accel.YminusCent(c)*dt;
    
    Velocity.N(c+1)  = Velocity.X(c+1)*cos(Heading(c+1));
    Velocity.E(c+1)  = Velocity.X(c+1)*sin(Heading(c+1));
    
    Position.North(c+1)   = Position.North(c)+Velocity.N(c+1)*dt;
    Position.East(c+1)    = Position.East(c)+Velocity.E(c+1)*dt;
end
dataplot1 = subplot(2,1,1);
plot(Accel.Time,Velocity.X(1:end-1));
hold on 
plot (Accel.Time,Velocity.Y(1:end-1));
legend('Velocity X','Velocity Y')
dataplot2 = subplot(2,1,2);
plot(Accel.Time,Heading(1:end-1));

figure
plot (Position.East,Position.North,'.')
hold on

linkaxes ([dataplot1,dataplot2],'x')
toc