clc, clear all, close all
load ('Clean/David1')

    d1 = designfilt('lowpassiir','FilterOrder',12, ...
        'HalfPowerFrequency',0.15,'DesignMethod','butter');
    Gyro.X = filtfilt(d1,Gyro.X);
    dt = Gyro.Time(2)-Gyro.Time(1);
    roll = 0;
    Gyro.X = Gyro.X - mean(Gyro.X (50:200));
    for c=1:length(Gyro.X)
        if Gyro.Time(c) < 2
            Gyro.X(c) = 0;
        end
%         if c >6
%         if abs(Gyro.X(c))<.05 && abs(Gyro.X(c-5))<.05
%             Gyro.X(c) = 0;
%         end
%         end
        roll(c+1) =  roll(c)+Gyro.X(c)*dt;
    end

    subplot (2,1,1)
    plot (Gyro.Time,Gyro.X)
    subplot(2,1,2)
    plot (Gyro.Time,roll(1:end-1))