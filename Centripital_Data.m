%Find centripital odata V1
clc, clear all, close all
load ('Clean/Jantzen2')


d1 = designfilt('lowpassiir','FilterOrder',12, ...
    'HalfPowerFrequency',0.15,'DesignMethod','butter');
Gyro.Z = filtfilt(d1,Gyro.Z);
dt = Gyro.Time(2)-Gyro.Time(1);
heading = 0;
for c=1:length(Gyro.Z)
    if Gyro.Time(c) < 2
        Gyro.Z(c) = 0;
    end
    if c >6
        if abs(Gyro.Z(c))<.05 && abs(Gyro.Z(c-5))<.05
            Gyro.Z(c) = 0;
        end
    end
    heading(c+1) =  heading(c)+Gyro.Z(c)*dt;
end
yawRate = Gyro.Z;


Accel.X(1:75) = 0;
Accel.X = Accel.X - mean(Accel.X(76:500));
Accel.X = filtfilt(d1,Accel.X);
Velocity.X = 0; Velocity.Y = 0;
for c = 1:length(Accel.Time)
    Velocity.X(c+1)  = Velocity.X(c) + Accel.X(c) * dt;
end


subplot (2,1,1)
plot (Gyro.Time,yawRate)
title('yaw rate over time')
subplot(2,1,2)
plot (Gyro.Time,heading(1:end-1))
title('heading over time')

d1 = designfilt('lowpassiir','FilterOrder',12, ...
    'HalfPowerFrequency',0.15,'DesignMethod','butter');
Gyro.X = filtfilt(d1,Gyro.X);
dt = Gyro.Time(2)-Gyro.Time(1);
roll = 0;
Gyro.X(1:79) = 0;
Gyro.X = Gyro.X - mean(Gyro.X (80:500));
for c=1:length(Gyro.X)
    roll(c+1) =  roll(c)+Gyro.X(c)*dt;
end
figure
subplot (2,1,1)
plot (Gyro.Time,Gyro.X)
title('roll rate over time')
subplot(2,1,2)
plot (Gyro.Time,roll(1:end-1))
title('roll over time')
figure
plot (Accel.Time, Accel.Z)


Accel.Y(1:80)=0;
Position.North = 0; Position.East = 0;
addFix = Velocity.X(1:end-1).* Gyro.Z + 9.81* roll(1:length(Accel.Y));
Accel.YminusCent = Accel.Y - addFix ;%- 9.81* roll(1:length(Accel.Y));

plot (Accel.Time,addFix)
 hold on
 plot (Accel.Time,Accel.Y,':')
plot(Accel.Time,Accel.YminusCent,':')
% 
% for c = 1:length(Accel.Time)
%     Velocity.Y(c+1)  = Velocity.Y(c) +Accel.YminusCent(c)*dt;
%     
%     Velocity.N(c+1)  = Velocity.X(c+1)*cos(heading(c+1));
%     Velocity.E(c+1)  = Velocity.X(c+1)*sin(heading(c+1));
%     
%     Position.North(c+1)   = Position.North(c)+Velocity.N(c+1)*dt;
%     Position.East(c+1)    = Position.East(c)+Velocity.E(c+1)*dt;
% end
% dataplot1 = subplot(2,1,1);
% plot(Accel.Time,Velocity.X(1:end-1));
% hold on 
% plot (Accel.Time,Velocity.Y(1:end-1));
% legend('Velocity X','Velocity Y')
% dataplot2 = subplot(2,1,2);
% plot(Accel.Time,heading(1:end-1));
% 
% figure
% plot (Position.East,Position.North,'.')
% hold on

% linkaxes ([dataplot1,dataplot2],'x')
