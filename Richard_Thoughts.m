clc, close all, clear all

dataStr = 'Leo2';
showPlots = 1;
includeApprox = 1;

load(['Raw_Data_And_Parsing/Clean/',dataStr])
dt = Accel.Time(2)-Accel.Time(1);

% Select range for analysis and for calibration
if strcmp(dataStr,'Leo1') %crosses 0.....?
    range = (92000:103000);
    stillRange = 33000:60000;
end
if strcmp(dataStr,'Leo2')
    range = (88500:104700);
    range = (210000: 250000);
    stillRange = 80:70000;
end
if strcmp(dataStr,'David1')
    range = (122000:145000);
    stillRange = 1000:80000;
end
if strcmp(dataStr,'David2')
    range = (47700:66000);
    stillRange = 10:40000;
end
if strcmp(dataStr,'Jantzen2') %CURRUPTED
    range = (8000:90000);
    stillRange = 50:17000;
end

% Calibrate and check calibration
if showPlots
    figure
    hold on;
    plot(Accel.Time(range),Accel.X(range))
end

Accel.X = calibrate(Accel.X,Accel.Time,stillRange);
if 0
    plot(Accel.Time(range),Accel.X(range),'-.')
end


time = Accel.Time;
tic

% Get filtered yaw rate
yawRate = zeros(size(time));
run('lee_thoughts/getYawRate')
if showPlots
    plot1 = figure;
    plot(time(range),yawRate(range))
    title('Yaw Rate as a function of Time')
    xlabel('time (s)'), ylabel('Yaw Rate (deg/s)')
end

% Get velocity with omega and accelX
AvgOmega = zeros(size(time));
ismoving = zeros(size(time));
run('lee_thoughts/getVelOmega')
run('lee_thoughts/getVelocity')
if 0
    plot2 = figure;
    plot(time(range),VelocityOmega(range));
    hold on
    plot (time(range),Velocity.X(range))
    title('Velocity X as a function of Time')
    xlabel('Time(s)');ylabel('Velocity X (m/s)');
    legend('Via WheelSpeed','Via integration')
    
end

% Compute centripetal accel
R = 16.7;
CentA = (VelocityOmega.^2/R)/9.81;
Alat=yawRate.*VelocityOmega/9.81;
% plot3 = figure;
% ha(1) = subplot(3,1,1); hold on;
figure
if includeApprox
    plot(time(range),CentA(range))
    [coeffs,centApprox] = LeastSq(CentA(range),time(range),2);
    hold on
    plot(time(range),centApprox);
    hold on
    %plot(time(range),Alat(range));
end

Accel.Y = calibrate(Accel.Y,Accel.Time,stillRange);
plot (time(range), Accel.Y(range)/9.81,':')
title ('Centripetal Acceleration as a function of time')
xlabel('Time(s)');ylabel('Centripetal Acceleration(gs)');
legend('v^2/R','v^2/R fit','Ay')

% Integrate to get roll
roll = zeros(size(time));
run('lee_thoughts/findRoll')
roll = roll*180/pi;

% ha(2) = subplot(3,1,2); hold on;
figure
plot(time(range),roll(range))
hold on
if includeApprox
    [coeffs,rollApprox] = LeastSq(roll(range),time(range),2);
    plot(time(range),rollApprox);
end

title ('Roll as a function of time')
xlabel('Time(s)');ylabel('Roll angle (deg)');
legend('Roll','Roll Fit')

% Solve for roll rate
rollRate = zeros(size(time));
a = 1;
for c = 1:length(roll)
    if abs(roll(c))> 0.01 && abs(CentA(c))>.025
        plotr.roll(a) = roll(c);
        plotr.Accel(a) = CentA(c);
        a = a+1;
    end
end
for c = range
    if abs(CentA(c)) < 0.1
        rollRate(c) = 0;
    else
        rollRate(c) = abs(roll(c)/CentA(c));
        if includeApprox
            rollRateApprox(c) = abs(rollApprox(c-range(1)+1)/centApprox(c-range(1)+1));
        end
    end
end
b = 1;
for c = range
    if isfinite(rollRate(c)) && rollRate(c) >0.1
        rollStiffness(b) = rollRate(c);
        b = b+1;
    end
end
figure
% mean(rollStiffness)
% ha(3) = subplot(3,1,3); hold on;
plot (time(range),rollRate(range));
hold on
if includeApprox
    plot(time(range),rollRateApprox(range));
end

title ('Roll Rate over time')
xlabel('Time(s)');ylabel('Roll Rate (deg/G)');
legend('Roll Rate','Roll Rate Fit')
% linkaxes(ha,'x');

if 0
    % just for kicks.. trying to make it look more like a spring curve but its
    % weird bc its not really force on y axis..
    plot4 = figure;
    plot(roll(range),1./rollRate(range));
    xlabel('roll (deg)'); ylabel('roll rate (g/deg)');
end
figure; hold on;
plot(CentA(range),roll(range),centApprox,rollApprox)
plot(centApprox,rollApprox)
[asdf as] = LeastSq(roll(range),CentA(range),2);
plot(CentA(range),as)
txt = sprintf('Raw fit = %2.2f (deg/g)',(asdf(2)));
legend('Raw Data',txt,'Least Squares Data, 0.78 (deg/g)');
title('Roll vs Centripetal Acceleration')



toc